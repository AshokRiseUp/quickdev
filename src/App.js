import React from "react";
import { BrowserRouter as Router } from "react-router-dom";
import Header from "./Components/Header/Header";
import Footer from "./Components/Footer/Footer";
import { RouteProvider } from "./Route/RouteProvider";
import './App.scss';
import logo from './Assets/images/Logo_Def_RGB.png';


function App() {  
  const footerParams = {
    title : "Sample footer text",
    className : "footer",
    img: logo,
    copyRight: <span>&#169; 2021 - Sample copyright</span>
  }

  return (
    <Router>
      <Header text="SynEdge tooling" />
        <div className="body-full-height">
          <RouteProvider />
        </div>
      <Footer {...footerParams} />
    </Router>
  );
}

export default App;
