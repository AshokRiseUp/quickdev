import { render, screen, waitFor } from '@testing-library/react';
import App from '../App';

test('ensuring that we have proper app', async () => {
  render(<App />);
  await waitFor(() => { 
      const linkElement = screen.getByText(/SynEdge tooling/i);
      expect(linkElement).toBeInTheDocument();
  })
});
