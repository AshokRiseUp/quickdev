import { useEffect, useState } from "react";
import { useHistory, useLocation } from "react-router-dom";
import { isAuthenticated } from "../MockAPI/restCalls";

function AuthChecker(props) {
  const history = useHistory();
  const location = useLocation();
  const [renderChildren, setRenderChildren] = useState(false);

  useEffect(() => {
      setRenderChildren(false);
      if(!isAuthenticated() 
      && (location.pathname.indexOf("login") === -1
      && location.pathname.indexOf("forgot-password") === -1)){
          history.push("/login");
      }else{
          setRenderChildren(true);
      } 
  })
  
  return !renderChildren ? "" : props.children;
}

export default AuthChecker;

