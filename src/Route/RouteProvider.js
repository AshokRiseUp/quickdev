import { routeConfig } from "./routeConfig";
import { Route, Switch } from "react-router-dom";
import AuthChecker from "./AuthChecker";

export const RouteProvider = () => {
  return (
    <AuthChecker>
      <Switch>
        {routeConfig.map((routeParams, index) => {
            return <Route key={'route' + index} {...routeParams} />
        })}
      </Switch>
    </AuthChecker>
  );
};


