const PageNotFound = () => <h1>OOPS! Page not found.</h1>;

export default PageNotFound;