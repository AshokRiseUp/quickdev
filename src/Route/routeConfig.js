import Home from "../Components/Home/Home";
import PageNotFound from "./PageNotFound";

export const routeConfig = [
{
    component: Home,
    exact: false,
    path: "/Home",
    private: false
},
{
    component: Home,
    exact: true,
    path: "/",
    private: false
},
{
    component: PageNotFound
}]