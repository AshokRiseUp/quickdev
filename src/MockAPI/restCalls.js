const {apiURL}  = require("../app.config"); 

export const login = async (loginData, callBack) => {
    await fetch(`${apiURL}/login`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body:JSON.stringify(loginData)
    }).then(response => response.json())
    .then(data => callBack(data));
};

export const register = async (registerData, callBack) => {
    await fetch(`${apiURL}/register`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body:JSON.stringify(registerData)
    }).then(response => response.json())
    .then(data => callBack(data));
};

export const editCustomer = async (customerId, payload, callBack) => {
    await fetch(`${apiURL}/users/${customerId}`, {
        method: 'PUT',
        headers: {
            'Content-Type': 'application/json'
        },
        body:JSON.stringify(payload)
    }).then(response => response.json())
    .then(data => callBack(data));
};

export const addCustomer = async (payload, callBack) => {
    await fetch(`${apiURL}/users`, {
        method: 'POST',
        headers: {
            'Content-Type': 'application/json'
        },
        body:JSON.stringify(payload)
    }).then(response => response.json())
    .then(data => callBack(data));
};

export const deleteCustomer = async (customerId, callBack) => {
    await fetch(`${apiURL}/users/${customerId}`, {
        method: 'DELETE'
    }).then(response => response.text())
    .then(data => callBack(data));
};

export const listCustomers = async (page, callBack) => {
    await fetch(`${apiURL}/users?page=${page}`, {
        method: 'GET'
    }).then(response => response.json())
    .then(data => callBack(data));
};

export const isAuthenticated = () => {
    return localStorage.length > 0;
}

