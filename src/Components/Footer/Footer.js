import React from "react";
import "./Footer.scss";

const Footer = (props) => {
    const classProperty = (!!props && !!props.className) ? props.className : '';
    const imageProperty =  (!!props && !!props.img) ? <img src={props.img} title={props.title} /> : props.title;
    const copyrightProperty = (!!props && !!props.copyRight) ?  props.copyRight : "";
    const finalProperties = {
        className : classProperty
    };

    return <React.Fragment>
        <footer {...finalProperties}>
            <span className="footer-logo" data-testid="footer-copy-logo">{imageProperty}</span><br />
            <span className="footer-copy" data-testid="footer-copy-id">{copyrightProperty}</span>
        </footer>
    </React.Fragment>
}

export default Footer; 