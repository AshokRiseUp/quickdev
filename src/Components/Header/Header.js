import React from 'react';
import { Link } from "react-router-dom";
import "./header.scss";
import logoImage from "../../Assets/images/Logo_Def_RGB.png"

const Header = () => {
    return (<header className="header">
        <Link to="/" className="logo">
          <img src={logoImage}  title="Sample logo" alt="Sample logo" />
        </Link>
        <div className="header-right">
          <Link to="/" className="link">Home</Link>
          <Link to="/" className="link">Contact</Link>
        </div>
    </header>)
}
    
export default Header;